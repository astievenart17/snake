package lecode;
import java.util.LinkedList;
import java.util.List;
import static lecode.Sketch.*;

public  class PuzzlesData {

   private List < Object[][] >       lesPuzzlesData = new LinkedList(); 

   public PuzzlesData() {
   
         lesPuzzlesData.clear();
   }
   
   public  void initViaCodeInterne() {
     
   
       
    /////////////////////////////////////////////////////////
      
    Object[][]               dataPz00 = {
                                          {"A",1,1,0,VERT}, {"A",3,5,0,VERT}, {"A",3,1,0,VERT}, {"B",0,3,0,ROUGE}, {"B",3,3,3,ROUGE},
                                          {"C",0,0,0,BLEU}, {"C",4,3,1,BLEU}, {"D",0,2,0,JAUNE},{"E",2,1,1,VIOLET}, {"E",4,0,3,VIOLET}
                                        };  
    lesPuzzlesData.add(dataPz00);
   
    /////////////////////////////////////////////////////////  
  
    Object[][]               dataPz01  ={
                                          {"D",1, 0, 1, JAUNE} , {"D",2, 0, 1, JAUNE}, {"B",2, 1, 2, ROUGE} , {"B",4, 4, 2, ROUGE}, 
                                          {"G",2, 3, 1, ORANGE}, {"A",5, 3, 0, VERT} , {"E",0, 3, 2, VIOLET}, {"E",4, 0, 1, VIOLET},
                                          {"E",0, 5, 2, VIOLET}
                                        };
    lesPuzzlesData.add(dataPz01);
   
    ////////////////////////////////////////////////////////
   
    Object[][]               dataPz02  ={
                                          {"H",4,0,1, ROUGE}, {"I",3,3,0, VERT}, {"I",2,1,0, VERT}, {"C",0,0,0, BLEU},
                                          {"D",4,0,1, JAUNE}, {"D",5,4,1, JAUNE},{"D",1,3,1, JAUNE}, {"E",0,3,1, VIOLET}
                                        };   
    lesPuzzlesData.add(dataPz02);
    
    /////////////////////////////////////////////////////////
    
    Object[][]               dataPz03  = {
                                           {"I",2,1,0, JAUNE}, {"A",3,5,0, VERT}, {"B",4,0,0, VERT}, {"C",0,3,0, ROUGE},
                                           {"F",3,2,0, ORANGE}, {"B",0,0,0, BLEU},{"I",4,4,1, BLEU}, {"D",0,2,0, JAUNE}
                                         };   
    lesPuzzlesData.add(dataPz03);
 
    ////////////////////////////////////////////////////////
    
     Object[][]              dataPz04   = {
                                            {"G",3,0,1,BLEU}, {"B",3,2,3,ROUGE},{"B",4,4,3,ROUGE},{"B",2,3,3,ROUGE},{"B",1,4,3,ROUGE},
                                            {"A",5,0,3,BLANC},{"A",5,2,3,BLANC},{"A",5,3,3,BLANC},{"A",5,4,3,BLANC},{"A",3,5,3,BLANC},
                                            {"A",0,0,3,BLANC},{"A",1,1,3,BLANC},{"A",1,2,3,BLANC}
                                          };
     lesPuzzlesData.add(dataPz04);
     
     ////////////////////////////////////////////////////////
   Object[][]              dataPz05  = {
                                             {"I",1,0,0,BLEU}, {"B",3,2,1,ROUGE}, {"E",5,0,1,VIOLET}, {"I",4,4,1,BLEU}, {"A",0,5,1,NOIR},
                                             {"K",0,3,2,AQUA}, {"G",1,2,0,NOIR}, {"A",1,2,1,JAUNE},
      
                                          };
     lesPuzzlesData.add(dataPz05);
     
     /////////////////////////////////////////////////////////
  
 
     Object[][]               dataPz06  = {
                                             {"A",5,0,0,BLEU}, {"A",4,4,0,BLEU}, {"E",2,0,1,VERT}, {"E",3,0,1,VERT}, {"F",4,0,1,ROUGE},
                                             {"H",0,2,2,AQUA}, {"I",0,0,0,JAUNE}, {"B",3,4,3,VIOLET}, {"D",5,4,1,NOIR}
                                          };    
     lesPuzzlesData.add(dataPz06);

      /////////////////////////////////////////////////////////
    
      Object[][]                 dataPz07  = {
                                           {"A",5,0,0,BLEU}, {"A",2,3,0,BLEU}, {"A",0,5,0,BLEU}, {"B",0,0,0,VERT}, {"E",0,2,1,ROUGE},
                                           {"E",3,3,0,ROUGE}, {"F",1,4,0,JAUNE}, {"G",3,0,0,VIOLET}, {"I",4,4,0,NOIR}
                                         };  
      lesPuzzlesData.add(dataPz07); 
  
      ////////////////////////////////////////////////////////
  
       Object[][]              dataPz08  = { {"A" ,0,0,0,JAUNE}, {"G",0,0,0, ROUGE}, {"B",2,0,1, BLEU}, {"E",4,0,1, VERT}, {"K",0,2,0,NOIR},
                                             {"A",0,4,0,ORANGE}, {"I",2,2,0,VIOLET}, {"C",2,4,2,ROUGE}
                                           };
      lesPuzzlesData.add(dataPz08);
  
      ////////////////////////////////////////////////////////
    Object[][]              dataPz09   = {
                                           {"B",0,0,0,BLEU},{"B",0,4,3,BLEU},{"B",4,0,1,BLEU},{"B",4,4,2,BLEU}, {"K",4,1,0,ROUGE},
                                           {"K",0,2,0,ROUGE},{"D",1,1,3,JAUNE},{"D",4,3,1,JAUNE},{"A",3,4,1,NOIR},
                                       };   
       lesPuzzlesData.add(dataPz09);
  
       ////////////////////////////////////////////////////////
    
       Object[][]              dataPz10   = {
                                         {"F",3,4,0,BLEU},{"A",2,5,0,VERT},{"G",0,3,0,ROUGE},{"D",0,2,1,JAUNE},{"D",5,3,1,JAUNE},
                                         {"B",3,1,0,NOIR},{"C",4,0,1,ORANGE},{"J",0,0,1,VIOLET}
                                        };    
       lesPuzzlesData.add(dataPz10);
  
       ////////////////////////////////////////////////////////
  
       Object[][]               dataPz11  = {
                                        {"A", 3, 2, 0, VERT}, {"D", 2, 1, 1, JAUNE},{"F", 0, 3, 1, ROUGE},{"I", 4, 1, 0, BLEU},
                                        {"B", 0, 2, 1, AQUA}, {"H", 4, 3, 1, ORANGE},{"E", 1, 5, 0, VIOLET},{"K", 2, 3, 1, NOIR}
                                       };
       lesPuzzlesData.add(dataPz11);
  
       ////////////////////////////////////////////////////////  
  
  
       Object[][]              dataPz12    = {
                                         {"A",1,1,0,VERT}, {"A",3,5,0,VERT}, {"A",3,1,0,VERT}, {"C",0,0,0,BLEU}, {"C",4,3,1,BLEU},
                                         {"D",0,2,0,JAUNE},{"D",3,2,1,JAUNE},{"E",2,1,1,VIOLET}, {"E",4,0,3,VIOLET}, {"I",0,3,0,ROUGE}
                                        };    
       lesPuzzlesData.add(dataPz12); 
  
       ///////////////////////////////////////////////////////
 
       Object[][]               dataPz13    = {
                                          {"A",1,1,0,VERT}, {"A",3,5,0,VERT}, {"A",3,2,0,VERT}, {"B",0,3,0,ROUGE}, {"B",3,3,3,ROUGE},
                                          {"C",0,0,0,BLEU}, {"C",4,3,1,BLEU}, {"D",0,2,0,JAUNE}, {"E",2,1,1,VIOLET}, {"E",4,0,3,VIOLET}
                                         };    
       lesPuzzlesData.add(dataPz13); 
  
      /////////////////////////////////////////////////////////
       
       Object[][]              dataPz14  = {
                                           {"A",0,0,0,VERT}, {"A",4,2,0,VERT}, {"A",2,4,0,VERT}, {"H",3,0,0,ROUGE}, {"H",0,3,1,ROUGE},
                                           {"D",5,2,1,BLEU}, {"I",4,4,0,JAUNE}, {"G",0,0,0,VIOLET}, {"E",0,2,1,VIOLET}
                                      };

       lesPuzzlesData.add(dataPz14);
  
       ////////////////////////////////////////////////////////
    
        Object[][]               dataPz15    = {
                                                 {"H",0,0,3,VERT}, {"H",4,0,3,BLEU}, {"B",1,1,2,JAUNE}, {"B",3,3,2,VIOLET}, {"B",4,4,2,ROUGE},
                                                 {"I",1,3,1,BLANC}, {"J",0,5,1,NOIR}
                                         };   
       lesPuzzlesData.add(dataPz15);
  
       ////////////////////////////////////////////////////////       
  
       Object[][]              dataPz16    = {
                                              {"A",4,3,1,BLEU}, {"B",1,1,3,VERT}, {"B",4,1,3,VERT}, {"B",0,4,0,ROUGE}, {"B",3,3,3,ROUGE},
                                              {"B",0,0,0,BLEU}, {"B",0,2,3,JAUNE}, {"B",2,1,1,VIOLET}, {"B",4,0,1,VIOLET}
                                             };    
       lesPuzzlesData.add(dataPz16);
  
       ////////////////////////////////////////////////////////
    
       Object[][]              dataPz17  = {    
                                      };
       lesPuzzlesData.add(dataPz17);
  
       ////////////////////////////////////////////////////////
    
       Object[][]               dataPz18   = {
                                         {"C",0,2,0,BLEU}, {"A",2,1,3,VERT}, {"F",2,0,2,JAUNE}, {"I",0,0,0,VIOLET}, {"C",4,3,3,ROUGE},
                                         {"C",0,3,2,ORANGE}, {"B",4,1,3,VIOLET}, {"A",5,1,1,ROUGE}
      
                                      };  
       lesPuzzlesData.add(dataPz18);
  
       ////////////////////////////////////////////////////////
    
       Object[][]              dataPz19  = {
                                              {"A",0,2,0,BLEU}, {"B",3,1,1,VERT}, {"F",0,4,2,VERT}, {"G",1,1,0,VIOLET}, {"C",3,4,2,ROUGE},
                                              {"J",5,0,0,BLEU}, {"I",0,0,0,JAUNE}
                                            
                                      }; 
       lesPuzzlesData.add(dataPz19);
  
       ////////////////////////////////////////////////////////
    
       Object[][]              dataPz20    = {
                                               {"A",1,0,1,BLEU}, {"A",4,3,3,BLEU}, {"A",5,0,3,BLEU}, {"B",4,1,2,ROUGE}, {"C",3,3,3,NOIR},
                                               {"G",1,1,0,VERT}, {"J",0,0,2,JAUNE}, {"D",5,4,1,VIOLET}, {"F",1,3,1,ORANGE}
                                             };   
       lesPuzzlesData.add(dataPz20); 
  
       ////////////////////////////////////////////////////////
 
     Object[][]              dataPz21  = {
                                               {"A",3,5,0,VERT}, {"A",3,1,0,VERT}, {"B",0,1,2,ROUGE}, {"B",3,3,3,ROUGE}, {"B",0,4,3,ROUGE},
                                               {"C",0,0,0,BLEU}, {"C",4,3,1,BLEU}, {"E",2,1,1,VIOLET}, {"E",4,0,3,VIOLET},
                                      };
  
       lesPuzzlesData.add(dataPz21);
   
       ////////////////////////////////////////////////////////
    
      Object[][]             dataPz22    = {
                                              {"A",5,0,0,ROUGE}, {"B",0,0,0,JAUNE}, {"B",4,0,3,JAUNE}, {"B",4,4,2,JAUNE},
                                              {"E",3,3,3,VERT}, {"H",0,4,2,AQUA}, {"I",2,0,0,BLEU}, {"I",4,2,0,BLEU}
                                      };
                                         
       lesPuzzlesData.add(dataPz22);
   
        ////////////////////////////////////////////////////////// NON CONFORME
    
       Object[][]              dataPz23 = {
                                              {"A",1,1,0,VERT}, {"A",3,5,0,VERT}, {"A",3,1,0,VERT}, {"B",0,3,0,ROUGE}, {"A",3,3,3,ROUGE},
                                             {"D",0,0,0,BLEU}, {"D",4,3,1,BLEU}, {"D",0,2,0,JAUNE}, {"D",2,1,1,VIOLET}, {"D",4,0,3,VIOLET},{"C",0,4,2,BLEU}, {"D",5,2,3,VIOLET}, {"D",5,4,3,ROUGE}
                                      };  
       lesPuzzlesData.add(dataPz23);
   
       //////////////////////////////////////////////////////////
 
       Object[][]              dataPz24 = {
                                              {"M",0,0,0,ROUGE}, {"D",4,0,0,VERT}, {"D",4,2,0,VERT}, {"L",1,2,0,JAUNE}, {"E",5,3,1,VIOLET},
                                              {"B",3,2,3,BLEU}, {"A",0,4,3,NOIR}, {"E",3,1,0,VIOLET}, {"A",0,2,3,NOIR}, {"A",3,0,3,NOIR}
                                          };    
       lesPuzzlesData.add(dataPz24);
   
       ////////////////////////////////////////////////////////
    
       Object[][]              dataPz25  = {
                                               {"K",3,2,0,ROUGE}, {"H",2,1,3,BLEU}, {"A",5,5,0,VIOLET}, {"B",0,0,0,NOIR}, {"F",0,2,1,JAUNE}, {"A",5,2,0,VIOLET}, 
                                               {"D",1,4,0,VERT}, {"D",5,0,1,VERT}, {"E",0,5,0,ORANGE}, {"A",1,1,0,VIOLET}
                                      };
        lesPuzzlesData.add(dataPz25);
  
       ////////////////////////////////////////////////////////
    
        Object[][]              dataPz26  = {
                                               {"A",0,0,0,VERT}, {"A",0,5,0,VERT}, {"A",3,5,0,VERT}, {"C",2,1,1,NOIR}, {"D",1,0,0,ROUGE}, {"D",1,5,0,ROUGE},
	                                       {"D",4,5,0,ROUGE}, {"D",5,3,1,ROUGE}, {"E",3,0,2,BLEU}, {"E",0,2,1,BLEU}, {"I",4,1,0,AQUA}

                                      }; 
        lesPuzzlesData.add(dataPz26);
  
        ///////////////////////////////////////////////////////////
  
         Object[][]              dataPz27    = {
                                          {"A",3,0,1,BLEU}, {"C",0,0,0,VERT}, {"C",1,1,3,VERT}, {"G",2,1,0,ROUGE}, {"E",5,3,3,VIOLET},
                                          {"F",0,3,1,BLEU}, {"I",2,4,3,JAUNE}
                                        };             
         lesPuzzlesData.add(dataPz27);
  
          ////////////////////////////////////////////////////////
 
       Object[][]              dataPz28  = {
                                          {"A", 0, 0, 0, VERT}, {"B", 4, 1, 3, AQUA}, {"D", 0, 4, 1, JAUNE}, {"D", 1, 4, 0, JAUNE},
                                          {"F", 0, 1, 3, ROUGE}, {"G", 2, 2, 0, VERT}, {"I", 2, 0, 0, BLEU}, {"K", 3, 4, 1, NOIR}
      
                                      };
         lesPuzzlesData.add(dataPz28);
   
         ////////////////////////////////////////////////////////
 
         Object[][]              dataPz29  = {
      
                                      };    
          lesPuzzlesData.add(dataPz29);
   
          ////////////////////////////////////////////////////////
    
          Object[][]              dataPz30  = {
      
                                      };    
          lesPuzzlesData.add(dataPz30);
   
          ////////////////////////////////////////////////////////
    
          Object[][]              dataPz31  = {
                                                {"K",0,0,1,NOIR},{"E",1,4,0,JAUNE},{"K",3,1,1,NOIR},{"H",0,3,3,NOIR},{"I",4,4,0,JAUNE},
                                                {"A",1,5,1,NOIR},{"A",2,5,1,JAUNE},{"A",3,5,1,NOIR},{"B",1,1,2,JAUNE}
                                      };
          lesPuzzlesData.add(dataPz31);
   
   }
   
   public  void initViaFichierCSV()  {
   } 
   public  void initViaBase()        {
   }
   public  void initViaFichierXML()  {
   }
   public  void initViaApiRest()     {
   }
   
   public List<Object[][]> getLesPuzzlesData() {
        return lesPuzzlesData;
  }       
}

