package lecode;

class CompFigA   implements CompFig{
 
  //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
          
  int    largeur[]={1,1,1,1};  
  int    hauteur[]={1,1,1,1};

  /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

  int[][][]     casesARemplir=
  {
   { {0,0} }, // ETAT 0
   { {0,0} }, // ETAT 1
   { {0,0} }, // ETAT 2
   { {0,0} }  // ETAT 3 
  };
 
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  
  int[][][]     casesBloquantes=
  {
    {},        // ETAT 0
    {},        // ETAT 1
    {},        // ETAT 2
    {}         // ETAT 3 
  }; 

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

  int [][][][]  voisins=
  {
   { // A DROITE
    { {1,0} }, // ETAT 0 
    { {1,0} }, // ETAT 1
    { {1,0} }, // ETAT 2
    { {1,0} }  // ETAT 3
   },
   {// A GAUCHE
    { {-1,0} }, // ETAT 0 
    { {-1,0} }, // ETAT 1
    { {-1,0}  }, // ETAT 2
    { {-1,0} }  // ETAT 3
   },
   {// EN HAUT
    { {0,-1} }, // ETAT 0 
    { {0,-1} }, // ETAT 1
    { {0,-1}  }, // ETAT 2
    { {0,-1} }  // ETAT 3
   },
    {// EN BAS
    { {0,1}  }, // ETAT 0 
    { {0,1}  }, // ETAT 1
    { {0,1}  }, // ETAT 2
    { {0,1}  }  // ETAT 3
   }
 };
 
 ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
 
 String[]   nvType={"-","-","-","-"};
 int[]      nvEtat={ 0 , 0 , 0 , 0 };
 
 int[][][]  casesARaboter=
            {
              { {0,0} },
              { {0,0} },
              { {0,0} },
              { {0,0} }
            };
            
 int[][]  nvPosition=
            {
              {0,0},
              {0,0},           
              {0,0},
              {0,0}
            };
    
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
 
  //<editor-fold defaultstate="collapsed" desc="getters  ">
  @Override
  public int[][][]     getCasesARemplir()   { return casesARemplir; }
  
  @Override
  public int[][][]     getCasesBloquantes() { return casesBloquantes; }
  
  @Override
  public int[][][][]   getVoisins()         { return voisins; };
  
  
  @Override
  public String[]      getNvType(){return nvType;}
  
  @Override
  public int[]         getNvEtat(){return nvEtat;}
  
  @Override
  public int[][][]     getCasesARaboter(){return casesARaboter;}
  
  @Override
  public int[][]       getNvPosition(){return nvPosition;}
  
  
  @Override
  public int[]         getLargeur(){return largeur;}
  
  @Override
  public int[]         getHauteur(){return hauteur;}
  //</editor-fold>

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
 
}



