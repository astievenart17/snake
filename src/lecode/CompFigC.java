package lecode;

public class CompFigC  implements CompFig{
 
  ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  
  int        largeur[]={3,2,3,2}; // largeur  de la figure en nombre de cases  dans les états 0, 1, 2, 3
  int        hauteur[]={2,3,2,3}; // hauteur  de la figure en nombre de cases  dans les états 0, 1, 2, 3

  ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////  
  
 
  int[][][]     casesARemplir=   //  [ etat][ligne][colonne] 
  {                                                                                              
    { {0,0},{1,0},{2,0},{0,1}},  // ETAT 0   4 cases de la grille: [col=0, lig=0] [col=1, lig=0] [col=2, lig=0] [col=0, lig=1]    
    { {0,0},{1,0},{1,1},{1,2} }, // ETAT 1   4 cases de la grille: [col=0, lig=0] [col=1, lig=0] [col=1, lig=1] [col=1, lig=2]  
    { {2,0},{2,1},{1,1},{0,1} }, // ETAT 2
    { {0,0},{0,1},{0,2},{1,2} }  // ETAT 3 
  };
   
  ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// 
   
   int[][][]     casesBloquantes=  // cases qui doivent-être libres(non occupées par une autre figure)
                                   // pour pouvoir passer à l'état suivant ( +1 modulo 4) 
   {
     { {1,1},{1,2}},          // ETAT 0  2 cases  doivent-être libres : [col=1, lig=1] [col=1, lig=1]  
     { {0,1},{2,0},{2,1} },   // ETAT 1
     { {0,0},{0,2},{1,2} },   // ETAT 2  
     { {1,0},{2,0}  }         // ETAT 3
   };
 
 
  //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  
   int [][][][]  voisins=   // cases bordant la figure
   {
     { // A DROITE
       { {3,0}, {1,1} },           // ETAT 0 2 cases voisines à droite: [col=3, lig=0] [col=1, lig=1]    
       { {2,0}, {2,1},  {2,2}},    // ETAT 1 3 cases voisines à droite: [col=2, lig=0] [col=2, lig=1]  [col=2, lig=2] 
       { {3,0}, {3,1} },           // ETAT 2
       { {1,0}, {1,1},{2,2}}       // ETAT 3
     },
     {// A GAUCHE
       { {-1,0}, {-1,1} },         // ETAT 0 
       { {-1,0}, { 0,1},{ 0,2}  }, // ETAT 1
       { {1,0} , {-1,1} },         // ETAT 2
       { {-1,0}, {-1,1} , {-1,2} } // ETAT 3
     },
     {// EN HAUT
       { {0,-1}, {1,-1}, {2,-1} }, // ETAT 0 
       { {0,-1}, {1,-1} },         // ETAT 1
       { {0,0}, {1,0} , {2,-1}},   // ETAT 2
       { {0,-1}, {1,1} }           // ETAT 3
     },
     {// EN BAS
       { {0,2}, {1,1} , {2,1}},    // ETAT 0 
       { {0,1}, {1,3} },           // ETAT 1
       { {0,2}, {1,2} , {2,2}},    // ETAT 2
       { {0,3}, {1,3} }            // ETAT 3
   }   
  };
  
/////////////////////////////////// /////////////////////////////////////////////  
  
 String[]   nvType={"E","B","A","D"};  // LA FIGURE CHANGE DE TYPE
 int[]      nvEtat={ 0 , 1 , 0 , 1};   // DANS UN ETAT DONNÉ
 int[][][]  casesARaboter=             // CASES QUI DOIVENT DISPARAITRE LORS d'UN RABOTAGE DU BAS DE CETTE PIECE
            {
              { {0,1} },                // ETAT 0  
              { {1,2} },                // ETAT 1
              { {0,1}, {1,1}, {2,1} },  // ETAT 2
              { {0,2},{1,2} }           // ETAT 3
            };
            
 int[][]    nvPosition=                // LE POINT D'ANCRAGE N'EST PAS FORCEMMENT [COL=0, LIG=0 ]
            {
              {0,0},     // ETAT 0
              {0,0},     // ETAT 1      
              {2,0},     // ETAT 2 
              {0,0}      // ETAT 3  
            };               
   
 ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
 
  //<editor-fold defaultstate="collapsed" desc="getters  ">
  @Override
  public int[][][]     getCasesARemplir()   { return casesARemplir; }
   
  @Override
  public int[][][]     getCasesBloquantes() { return casesBloquantes; }
  
  @Override
  public int[][][][]   getVoisins()         { return voisins; };
  
  
  @Override
  public String[]      getNvType(){return nvType;}
  
  @Override
  public int[]         getNvEtat(){return nvEtat;}
  
  @Override
  public int[][][]     getCasesARaboter(){return casesARaboter;}
  
  @Override
  public int[][]       getNvPosition(){return nvPosition;}
  
  
  @Override
  public int[]         getLargeur(){return largeur;}
  
  @Override
  public int[]         getHauteur(){return hauteur;}
  //</editor-fold>

 ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
}
