package lecode;
import controleur.Controleur;
import ihm.VueJeu;
import java.util.*; 
import java.awt.Color;
import processing.core.*;
import static processing.core.PApplet.print;
import static processing.core.PApplet.println;

public class Sketch  extends PApplet{  
 
///////////////   DEFINITION DES STRUCTURES DE DONNEES ///////////////

////////////////////////////////////////////////////////////////////// 
// TABLE ASSOCIATIVE DES COMPORTEMENTS DES FIGURES
// CLE:    UNE CHAINE DE CARACTÈRES ( AB, B,C,  ... AA,AB,  ...) 
// VALEUR: UN  OBJET  IMPLEMENTANT L'INTERFACE ComFig  
 
 static Map<String, CompFig>     comps   = new HashMap(); 
  
 /////////////////////////////////////////////////////////////////////
 
/////////  MATRICE D'OCCUPATION DES CASES DE LA GRILLE ////////////// 
 
//   TABLEAU À DEUX DIMENSIONS 
//   PREMIER  INDICE: INDICE LIGNE
//   DEUXIÈME INDICE: INDICE COLONNE
//   VAUT NULL SI LA CASE DE LA GRILLE N'EST PAS OCCUPÉE
//   POINTE VERS L'OBJET DE LA CLASSE FIGURE QUI OCCUPE LA CASE
//   UNE FIGURE PEUT OCCUPER PLUSIEURS CASES 
////////////////////////////////////////////////////////////////////

 Fig [][]                        matrice;
 
 ///////////////////////////////////////////////////////////////////
 
 //////////// LISTE DES FIGURE PRÉSENTES SUR LA GRILLE//////////////
 
 List<Fig>                       lesFigs;
 
 ////////////////////////////////////////////////////////////////////
 
 /////////////  FIGURE ACTUELLEMENT SÉLECTIONNÉE ///////////////////
 
 static Fig                      figCour;     
    
////////////////////////////////////////////////////////////////////
 
///////////////////////////////////   DIMENSIONS    ////////////////

 int                          LGCASES, HTCASES, DIMCASE;

///////////////////////////  IDENTIFIANT FIGURE    /////////////////

 static int                   idSuivant=1;    

//////////////////////////  CONSTANTES DIRECTIONS  /////////////////

 static final int             DROITE=0, GAUCHE=1, HAUT=2, BAS=3;

///////////////////////// DEFINITION DES COULEURS //////////////////
 
 static Color ROUGE      = Color.RED;
 static Color VERT       = Color.GREEN;
 static Color BLEU       = Color.BLUE;
 static Color JAUNE      = Color.YELLOW;
 static Color VIOLET     = Color.MAGENTA;
 static Color AQUA       = Color.cyan;
 static Color NOIR       = Color.BLACK;
 static Color OUTREMER   = Color.BLUE;
 static Color ORANGE     = Color.ORANGE;
 static Color VERTPALE   = Color.GREEN;
 static Color BLANC      = Color.WHITE;
       
 /////////////////////  INITIALISATION DES COMPORTEMENTS   /////////////////////////////////////
    
 public static void initComportements(){
 
   comps.put("A", new CompFigA());
   comps.put("B", new CompFigB());
   comps.put("C", new CompFigC());
   comps.put("D", new CompFigD());
   comps.put("E", new CompFigE());
   comps.put("F", new CompFigF());
   comps.put("G", new CompFigG());
   comps.put("H", new CompFigH());
   comps.put("I", new CompFigI());
   comps.put("J", new CompFigJ());
   comps.put("K", new CompFigK()); 
   comps.put("L", new CompFigL()); 
   comps.put("M", new CompFigM());  
 }

///////////////////////////   CREATION   DES PUZZLES EN MEMOIRE               ////////////

 /////////  CETTE METHODE SERA DEFINIE DANS LES CLASSES FILLES DE LA CLASSE Sketch //////
 
 public  void initFigures(){ };    
 
/////////////////////////////////////  GESTION DE LA SOURIS   ///////////////////////////

 @Override
 public void mousePressed(){

  int mx=mouseX/DIMCASE,my=mouseY/DIMCASE; 
  
  figCour=matrice[mx][my]; 
   
  print("click en x="+mx+" y="+my);
  if   (figCour!=null){
       print(" ");
       afficheFigSel();
  }
  else    println( " Pas de figure sélectionnée");   
}        

/////////////////////////////////////  LOG CONSOLE            /////////////////////////////

 void        afficheFigSel(){

  if (figCour!=null){
    println( " Figure Id= "+figCour.id+" Type= "+figCour.type+
             " en x="+figCour.x+ " y="+figCour.y+" Etat= "
             +figCour.etat+ "  L="+figCour.largeur()+" H= "+figCour.hauteur()
             );
  } 
}

/////////////////////////////////////  DESSIN DU QUADRILLAGE  /////////////////////////////

 void        quadrillage(){

 stroke(200, 200, 200); 
 strokeWeight(1); 
  
 for (int x=0;x<DIMCASE*LGCASES; x+=DIMCASE){
    line(x,0,x,DIMCASE*HTCASES-1);
  }

 for(int y=0;y<DIMCASE*HTCASES;y+=DIMCASE){
    line (0,y,DIMCASE*LGCASES-1,y);
 }  
 
 strokeWeight(0.5f);
 stroke(180, 180, 180);

 line(0,0,DIMCASE*LGCASES,0);
 line(0,0,0,DIMCASE*HTCASES);
 line(DIMCASE*LGCASES-1,0,DIMCASE*LGCASES-1,DIMCASE*HTCASES-1);
 line(0,DIMCASE*HTCASES-1,DIMCASE*LGCASES-1,DIMCASE*HTCASES-1);

 stroke(200, 200, 200); 
 strokeWeight(1);
}
   
//  ATTRIBUTS DONNANT ACCES AU CONTRÔLEUR ET À LA VUE
 
 Controleur controleur;
 VueJeu     vue;

 public void terminer(){exit();}
 
 //<editor-fold defaultstate="collapsed" desc="gets & sets">
 
 public Controleur getControleur() {
     return controleur;
 }
 
 public void setControleur(Controleur controleur) {
     this.controleur = controleur;
 }
 
 public VueJeu getVue() {
     return vue;
 }
 
 public void setVue(VueJeu vue) {
     this.vue = vue;
 }
 
 //</editor-fold>
}



