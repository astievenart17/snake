
package lecode;

import java.util.*; 
import static lecode.Sketch.VERT;
import static lecode.Sketch.comps;
import static lecode.Sketch.figCour;
import static processing.core.PApplet.floor;
import static processing.core.PApplet.print;
import static processing.core.PApplet.println;

public class SketchTetris  extends Sketch{  
  
  int nIt=0;  
    
  public SketchTetris() {
        
        LGCASES =  12;
        HTCASES =  24;
        DIMCASE =  17;
        
        matrice= new Fig[LGCASES][HTCASES];
 }
 
 @Override
 public void initFigures(){
  
  lesFigs= new LinkedList();   

}    
 
 
 public void generationFigure (){
 
      
      String[] cles= new String[comps.keySet().size()];
      int i=0;
      for (String s : comps.keySet()){
          
          cles[i]=s;
          i++;
      }
     
      int n= floor(random(comps.keySet().size()));  
       
      Fig  fig  = new Fig(cles[n],2, 0, 0, VERT,  this);  
      fig.setEtiquette(false);
      lesFigs.add(fig);
      figCour=fig;
      
 }
 
 
 
 
////////////////////////////////////  SETUP                   ///////////////////////////////
    
 @Override
 public void setup(){
  
  size (LGCASES*DIMCASE, HTCASES*DIMCASE);
  smooth();
  frameRate(30);
  initFigures(); 
  noLoop();
}
    
////////////////////////////////////  BOUCLE DE DESSIN        //////////////////////////////

 @Override
 public void draw(){
    
  background(230);
  quadrillage();
  strokeWeight(2);
  
  for(Fig f : lesFigs)f.affiche();
      
   if (figCour!=null){
     if (nIt==8){ 
       if( !figCour.contactBordBas() && !figCour.contactBasFigure()) {
           
            figCour.depY(1);
       } 
       else{
                   
           generationFigure();

       }         
       nIt=0;
   }
   else nIt++;
  }
   
}

/////////////////////////////////////  GESTION CLAVIER        ///////////////////////////////

 @Override
 public void keyPressed(){

   boolean toucheActive=false;
   switch(key){
   
      
      case 's': // Démarrer chute
            loop(); if (figCour==null) generationFigure();
      break;  
       
    
      case 'f': // Test fin
            println( "fin ");  exit(); 
      break;    
      case 'r': // ROTATION
          toucheActive= true;
          if(figCour!=null && figCour.peutTourner() ){
            int etat=(figCour.etat+1)%4 ;
            figCour.oriente(etat) ;
            print( "rotation ");
        }         
      break;
      
      case 'm': // RABOTAGE
       if (figCour!=null)figCour.raboter();
      break;
      
      case 'l': 
       List<Fig> lar=new LinkedList();  
       for (Fig f : lesFigs) lar.add(f);
       for (Fig f : lar){
         f.raboter();
       }  
       for (Fig f : lesFigs) {
       
         if (f.y<HTCASES-1){
           f.depY(1);
         }
         else{
           lesFigs.remove(f);
         }  
       
        } 
      break;
      
      case 'a':  // ZOOM AVANT
       DIMCASE++;
       this.setSize ((LGCASES)*DIMCASE, (HTCASES)*DIMCASE);
       this.redraw();
      break;
       
      case 'd': // ZOOM ARRIERE
       DIMCASE--;
       this.setSize ((LGCASES)*DIMCASE, (HTCASES)*DIMCASE);
       this.redraw();
      break;
  }
     
   switch(keyCode){
    
      case 37: // NAV GAUCHE
       toucheActive= true;     
       if(figCour!=null){
           if( !figCour.contactBordGauche() && !figCour.contactGaucheFigure()) { 
                figCour.depX(-1); print( "dep gauche ");
            }
            else if(figCour.contactBordGauche()){
                    print( "collision bord gauche ");
                  }
                  else{
                    print( "collision gauche  avec autre figure ");
                  }
       }
       
      break;
      case 39:   // NAV DROITE
        toucheActive= true;
        if(figCour!=null){
           if( !figCour.contactBordDroit() && !figCour.contactDroitFigure()) { 
                figCour.depX(1); print( "dep droite ");
            }
            else if(figCour.contactBordDroit()){
                    print( "collision bord droit ");
                  }
                  else{
                    print( "collision droite  avec autre figure ");
                  }
       }
      break; 
      
   }
   if (toucheActive)afficheFigSel();
   
}
   
 
 @Override
 public void mousePressed(){}        
 
}
