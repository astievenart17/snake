
package lecode;

import java.util.*; 
import static lecode.Sketch.figCour;
import static processing.core.PApplet.print;
import static processing.core.PApplet.println;

public class SketchTaquin  extends Sketch{  
 
 
  int nIt=0;  
    
  public SketchTaquin() {
        
   LGCASES =  4;
   HTCASES =  4;
   DIMCASE =  105;  
   
   matrice= new Fig[LGCASES][HTCASES];
 }
 
 
////////////////////////////////////  SETUP                   ///////////////////////////////
    
 @Override
 public void setup(){
  
  size (LGCASES*DIMCASE, HTCASES*DIMCASE);
  smooth();
  frameRate(30);
   
  noLoop();
}
    
////////////////////////////////////  BOUCLE DE DESSIN        //////////////////////////////

 @Override
 public void draw(){
    
  background(230);
  quadrillage();
  strokeWeight(2);
 
  
 
}

/////////////////////////////////////  GESTION CLAVIER        ///////////////////////////////

 @Override
 public void keyPressed(){

   boolean toucheActive=false;
   switch(key){
   
      
      case 's': // Démarrer chute
            loop();
      break;  
       
    
      case 'f': // Test fin
            println( "fin ");  exit(); 
      break;    
      case 'r': // ROTATION
          toucheActive= true;
          if(figCour!=null && figCour.peutTourner() ){
            int etat=(figCour.etat+1)%4 ;
            figCour.oriente(etat) ;
            print( "rotation ");
        }         
      break;
      
      case 'm': // RABOTAGE
       if (figCour!=null)figCour.raboter();
      break;
      
      case 'l': 
       List<Fig> lar=new LinkedList();  
       for (Fig f : lesFigs) lar.add(f);
       for (Fig f : lar){
         f.raboter();
       }  
       for (Fig f : lesFigs) {
       
         if (f.y<HTCASES-1){
           f.depY(1);
         }
         else{
           lesFigs.remove(f);
         }  
       
        } 
      break;
      
      case 'a':  // ZOOM AVANT
       DIMCASE++;
       this.setSize ((LGCASES)*DIMCASE, (HTCASES)*DIMCASE);
       this.redraw();
      break;
       
      case 'd': // ZOOM ARRIERE
       DIMCASE--;
       this.setSize ((LGCASES)*DIMCASE, (HTCASES)*DIMCASE);
        this.redraw();
      break;
  }
     
   switch(keyCode){
    
      case 37: // NAV GAUCHE
       toucheActive= true;     
       if(figCour!=null){
           if( !figCour.contactBordGauche() && !figCour.contactGaucheFigure()) { 
                figCour.depX(-1); print( "dep gauche ");
            }
            else if(figCour.contactBordGauche()){
                    print( "collision bord gauche ");
                  }
                  else{
                    print( "collision gauche  avec autre figure ");
                  }
       }
       
      break;
      case 39:   // NAV DROITE
        toucheActive= true;
        if(figCour!=null){
           if( !figCour.contactBordDroit() && !figCour.contactDroitFigure()) { 
                figCour.depX(1); print( "dep droite ");
            }
            else if(figCour.contactBordDroit()){
                    print( "collision bord droit ");
                  }
                  else{
                    print( "collision droite  avec autre figure ");
                  }
       }
      break; 
      
   }
   if (toucheActive)afficheFigSel();
   
}
   
 
//@Override
// public void mousePressed(){}        
 
}
