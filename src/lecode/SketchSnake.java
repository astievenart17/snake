package lecode;

import java.util.LinkedList;
import java.util.List;
import java.awt.*;
import java.util.*; 
import static lecode.Sketch.*;
import static lecode.Fig.*;

public class SketchSnake extends Sketch {
   
    
    public SketchSnake(){
      
        LGCASES = 30;
        HTCASES = 30;
        DIMCASE = 15;
        
        matrice = new Fig[LGCASES][HTCASES];
        }
    ///////////////////////////////////////////////////////////////////////////
    
    public void setup(){
        
        size (LGCASES*DIMCASE, HTCASES*DIMCASE);
        
    }
    
    public void draw(){
        
        background(230);
        quadrillage();
        
    }

    @Override
    public void initFigures() {
        Fig tete = new Fig{"A",15,15,0,Color.GREEN,this};
    }
    
    
                                     
    
}

