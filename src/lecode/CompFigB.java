package lecode;

class CompFigB   implements CompFig{

 /////////////////////////////////////////////////////////////////////////////  /////////////////////////////////////         
 
 int    largeur[] = {2,2,2,2};
 int    hauteur[] = {2,2,2,2}; 

 ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
 
 int[][][]     casesARemplir=
 {
   { {0,0},{1,0},{0,1} }, // ETAT 0
   { {0,0},{1,1},{1,0} },  // ETAT 1
   { {1,0},{1,1},{0,1} },  // ETAT 2
   { {0,0},{1,1},{0,1} }   // ETAT 3 
 };
  
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////// 
 
  int[][][]     casesBloquantes=
   {
    {{1,1}},
    {{0,1}},
    {{0,0}},
    {{1,0}}
  };
 
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
 
 int [][][][]  voisins=
 {
   { // A DROITE
    { {2,0}, {1,1} }, // ETAT 0 
    { {2,0}, {2,1} }, // ETAT 1
    { {2,0}, {2,1} }, // ETAT 2
    { {1,0}, {2,1} }  // ETAT 3
   },
   {// A GAUCHE
    { {-1,0}, {-1,1} }, // ETAT 0 
    { {-1,0}, { 0,1} }, // ETAT 1
    { {0,0} , {-1,1} }, // ETAT 2
    { {-1,0}, {-1,1} }  // ETAT 3
   },
   {// EN HAUT
    { {0,-1}, {1,-1} }, // ETAT 0 
    { {0,-1}, {1,-1} }, // ETAT 1
    { {0,0}, {1,-1} }, // ETAT 2
    { {0,-1}, {1,0} }  // ETAT 3
   },
    {// EN BAS
    { {0,2}, {1,1} }, // ETAT 0 
    { {0,1}, {1,2} }, // ETAT 1
    { {0,2}, {1,2} }, // ETAT 2
    { {0,2}, {1,2} }  // ETAT 3
   }
   
 };
 
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

  String[]     nvType={"D","D","A","A"};
  int[]        nvEtat={ 0 , 0 , 0 , 0};  
  int[][][]    casesARaboter=
               {
                 { {0,1} },
                 { {1,1} },
                 { {0,1},{1,1}},
                 { {0,1},{1,1} }
               };
  int[][]      nvPosition=
               {
                {0,0},
                {0,0},           
                {1,0},
                {0,0}
               };  
  
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
 
  //<editor-fold defaultstate="collapsed" desc="getters  ">
  @Override
  public int[][][]     getCasesARemplir()   { return casesARemplir; }
  
  @Override
  public int[][][]     getCasesBloquantes() { return casesBloquantes; }
  
  @Override
  public int[][][][]   getVoisins()         { return voisins; };
  
  
  @Override
  public String[]      getNvType(){return nvType;}
  
  @Override
  public int[]         getNvEtat(){return nvEtat;}
  
  @Override
  public int[][][]     getCasesARaboter(){return casesARaboter;}
  
  @Override
  public int[][]       getNvPosition(){return nvPosition;}
  
  
  @Override
  public int[]         getLargeur(){return largeur;}
  
  @Override
  public int[]         getHauteur(){return hauteur;}
  //</editor-fold>

 ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
}

