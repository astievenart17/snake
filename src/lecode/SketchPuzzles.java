package lecode;

import java.awt.Color;
import java.util.*; 
import static lecode.Sketch.figCour;
import static processing.core.PApplet.floor;
import static processing.core.PApplet.print;

public class SketchPuzzles  extends Sketch{  
        
  // LISTE DES PUZZLES
  // UN PUZZLE ÉTANT LUI-MÊME UNE LISTE DE FIGURES ( Fig ) 
    
  List< List<Fig> >  lesPuzzles= new LinkedList();   
  
  int                nbPuzzleEnCours;
  
  public SketchPuzzles() {
       
        //DIMENSIONNEMENT DE LA GRILLE 
      
        LGCASES = 6;
        HTCASES = 6;
        DIMCASE =70;
        
        // CRÉATION DE LA MATRICE D'OCCUPATION
        matrice= new Fig[LGCASES][HTCASES];     
   }
   
////////////////////////////////////   SETUP   ////////////////////////////////////
    
  @Override
  public void setup(){
  
    size (LGCASES*DIMCASE, HTCASES*DIMCASE);
    smooth();
    initFigures(); 
    selectionnerPuzzle(0);
}
 
  ///////////////////////////////      CRÉATION   DES PUZZLES EN MÉMOIRE   ///////

  @Override
  public void initFigures(){
         
      // CRÉATION D'UN lPzd OBJET QUI CONTIENDRA LA DÉFINITION DES PUZZLES 
      PuzzlesData lPzd=new PuzzlesData();
      
      // INITIALISATION DE DONNÉES A PARTIR DES DONNÉES INTERNES À L'OBJET lPzd
      lPzd.initViaCodeInterne();
      
      // PARCOURS DES DÉFINITIONS DE PUZZLE
      for ( Object[][] pzd : lPzd.getLesPuzzlesData()){
      
        //CRÉATION D'UN PUZZLE VIDE (QUI EST UNE LISTE DE FIGURES)
        List<Fig>   pz = new LinkedList();  
        
        // PARCOURS DES DESCRIPTIONS DE FIGURES DE L'OBJET pzd 
        for (Object[] descFig : pzd  ){
            
            // CONVERSION DES DONNEES SELON LE TYPE ATTENDU
            String  type    =  (String)  descFig[0];
            int     x       =  (Integer) descFig[1];
            int     y       =  (Integer) descFig[2];
            int     etat    =  (Integer) descFig[3];
            Color   couleur =  (Color)   descFig[4]; 
        
            // CRÉATION D'UNE FIGURE AVEC LES DONNEES CONVERTIES 
            Fig     fig =new Fig(type,x,y,etat,couleur,this);
           
            // AJOUT DE LA FIGURE AU PUZZLE
            pz.add(fig);    
        }
        
        // AJOUT DU PUZZLE À LA LISTE DES PUZZLES
        lesPuzzles.add(pz);
      }
}    
   

  //////////////////////////////       SELECTION ALEATOIRE D'UN NUMEROE DE PUZZLE   TOUCHE P  /////////
  
  private void selectionAleatPuzzle() {
     
        nbPuzzleEnCours= floor(random(lesPuzzles.size()));   
        selectionnerPuzzle(nbPuzzleEnCours);

 }
  
 /////////////////////////////         SELECTION SEQUENTIELLE CYCLIQUE  D'UN NUMERO DE PUZZLE  touche C incrémenter  touche X pour décrémenter 
 
  private void selectionSeqPuzzle(int sens) {
    
        if (sens==1){nbPuzzleEnCours++;} 
        else {
            if (nbPuzzleEnCours>0) nbPuzzleEnCours--;else nbPuzzleEnCours=lesPuzzles.size()-1;
        }
        
        nbPuzzleEnCours%=lesPuzzles.size();
       
        selectionnerPuzzle(nbPuzzleEnCours); 
  } 
 
     //////////////////////////////////    SELECTION D'UN PUZZLE   //////////////////////////////////////
  
     private void selectionnerPuzzle(int nbPuzzleEnCours){
  
       lesFigs=lesPuzzles.get(nbPuzzleEnCours); 
      
       if      (nbCases(lesFigs)!=25 && !lesFigs.isEmpty()){
             
             System.out.printf("Puzzle N° : %3d NON CONFORME\n",nbPuzzleEnCours);
             this.vue.setTitle("PUZZLE N° " +nbPuzzleEnCours+" NON CONFORME "+ nbCases(lesFigs)+" CASES");
        }
       else if (lesFigs.isEmpty() ) {
             
             System.out.printf("Puzzle N° : %3d VIDE\n",nbPuzzleEnCours);
             this.vue.setTitle("PUZZLE N° " +nbPuzzleEnCours+" VIDE");
        }
       else {
             
             System.out.printf("Puzzle N° : %3d OK\n",nbPuzzleEnCours);  
             this.vue.setTitle("PUZZLE N° " +nbPuzzleEnCours);
       }
       
       // REMETTRE A ZÉRO LA MATRICE D'OCCUPATION
       
       for(int c= 0;c<LGCASES;c++ )for(int l=0; l<HTCASES;l++) matrice[c][l]=null;
  
  }  
       
  //////////////////////////////  CALCUL NOMBRES DE CASE QU'OCCUPE UN PUZZLE  //////////////////////////////////////////////////  
     
    private int nbCases( List<Fig> pz){
  
     int nbCases=0;
     for ( Fig f : pz){ nbCases+=f.nbCases();}
    
     return nbCases;
    }
   
////////////////////////////////////   BOUCLE DE DESSIN   ////////////////////////

  @Override
  public void draw(){
    
    background(230);
    quadrillage();
    strokeWeight(2);
  
    for(Fig f : lesFigs) f.affiche();
   
}

////////////////////////////////////   GESTION CLAVIER    ////////////////////////

  boolean messageAffiche=false;
  
  @Override 
  public void keyPressed(){

   boolean toucheActive=false;
   switch(key){
   
        case 'c': // PUZZLE SUIVANT
           
            this.selectionSeqPuzzle(1);
      break;    
       
       
      case 'm':  //    Affichage d'un message
        if (!messageAffiche)this.getControleur().setMessage("Hello");
        else this.getControleur().setMessage("");
        messageAffiche=!messageAffiche;
      break;       
          
      case 'x': // PUZZLE PRECEDENT
           
            this.selectionSeqPuzzle(-1);
      break;      
           
      case 'p': // NOUVEAU PUZZLE PAR TIRAGE AU SORT
           
            this.selectionAleatPuzzle();
      break; 
       
      case 'r': // ROTATION
          toucheActive= true;
          if(figCour!=null && figCour.peutTourner() ){
            int etat=(figCour.etat+1)%4 ;
            figCour.oriente(etat) ;
            print( "rotation ");
          }         
      break;
      
      case 'a':  // ZOOM AVANT
       DIMCASE++;
       this.setSize ((LGCASES)*DIMCASE, (HTCASES)*DIMCASE);
       this.redraw();
      break;
       
      case 'd': // ZOOM ARRIERE
       DIMCASE--;
       this.setSize ((LGCASES)*DIMCASE, (HTCASES)*DIMCASE);
       this.redraw();
      break;
          
          
        
  }
     
  switch(keyCode){
    
      case 37: // NAV GAUCHE
       toucheActive= true;     
       if(figCour!=null){
           if( !figCour.contactBordGauche() && !figCour.contactGaucheFigure()) { 
                figCour.depX(-1); print( "dep gauche ");
            }
            else if(figCour.contactBordGauche()){
                    print( "collision bord gauche ");
                  }
                  else{
                    print( "collision gauche  avec autre figure ");
                  }
       }
       
      break;
      case 39:   // NAV DROITE
        toucheActive= true;
        if(figCour!=null){
           if( !figCour.contactBordDroit() && !figCour.contactDroitFigure()) { 
                figCour.depX(1); print( "dep droite ");
            }
            else if(figCour.contactBordDroit()){
                    print( "collision bord droit ");
                  }
                  else{
                    print( "collision droite  avec autre figure ");
                  }
       }
      break; 
      case 38:  // NAV HAUT
         toucheActive= true; 
         
         if(figCour!=null){
           if( !figCour.contactBordHaut() && !figCour.contactHautFigure()) { 
                figCour.depY(-1); print( "dep haut ");
            }
            else if(figCour.contactBordHaut()){
                    print( "collision bord haut ");
                  }
                  else{
                    print( "collision haut  avec autre figure ");
                  }
         }
      break;
      case 40: //  NAV BAS
         
          toucheActive= true;
         
          if(figCour!=null){
             
             if( !figCour.contactBordBas()) {
               if (!figCour.contactBasFigure()) { 
                   figCour.depY(1); print( "dep bas ");
               }
               else{
                    print( "collision bas avec autre figure ");
               }
             }  
             else{
              print( "collision bord bas");
             }  
          }
            
      break;
   }
   if (toucheActive)afficheFigSel();
   
 }
 
    
  ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  
    //<editor-fold defaultstate="collapsed" desc="gets & sets">
    
    public int getNbPuzzleEnCours() {
        return nbPuzzleEnCours;
    }
    //</editor-fold> 
}


