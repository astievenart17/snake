package lanceur;
import ihm.VueJeu;
import java.util.LinkedList;
import java.util.List;
import lecode.Sketch;
import lecode.SketchJDV;
import lecode.SketchLabyrinthe;
import lecode.SketchPuzzles;
import lecode.SketchTetris;
import lecode.SketchSnake;
import lecode.SketchLightOut;
import lecode.SketchTaquin;

public class LanceurVueJeu {

    public static void main(String[] args) {
          
        List<Sketch >lesSketches= new LinkedList();
        
        lesSketches.add(0, new SketchPuzzles());
        lesSketches.add(1, new SketchTetris());
        lesSketches.add(2, new SketchLabyrinthe());
        lesSketches.add(3, new SketchJDV());
        lesSketches.add(4, new SketchLightOut());
        lesSketches.add(5, new SketchTaquin());
        lesSketches.add(6, new SketchSnake());
                
        VueJeu v=new VueJeu(lesSketches);
        v.setVisible(true);
        
    }
}



