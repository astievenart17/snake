package controleur;
import UtilitaireBinding.Bean;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import lecode.Sketch;

public class Controleur extends Bean{

    private  Sketch sketchEnCours;
    
    private  String message;  // Propriété liée
   
    public void terminer(){
    
        System.out.println("TERMINAISON");  
        sketchEnCours.terminer();  
    }
    
    //<editor-fold defaultstate="collapsed" desc="PROPRIETES LIEES">
    
     public static final String PROP_MESSAGE = "message";

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        String oldMessage = this.message;
        this.message = message;
        propertyChangeSupport.firePropertyChange(PROP_MESSAGE, oldMessage, message);
    }
    private transient final PropertyChangeSupport propertyChangeSupport = new PropertyChangeSupport(this);

    public void addPropertyChangeListener(PropertyChangeListener listener) {
        propertyChangeSupport.addPropertyChangeListener(listener);
    }

    public void removePropertyChangeListener(PropertyChangeListener listener) {
        propertyChangeSupport.removePropertyChangeListener(listener);
    }
    
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="GETS & SETS">
    
    public Sketch getSketchEnCours() {
        return sketchEnCours;
    }

    public void setSketchEnCours(Sketch sketchEnCours) {
        this.sketchEnCours = sketchEnCours;
        
    }
    //</editor-fold> 
}



